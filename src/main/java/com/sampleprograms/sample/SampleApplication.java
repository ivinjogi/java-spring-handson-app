package com.sampleprograms.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleApplication.class, args);
		PrimeNumber primeNumber=new PrimeNumber();
		primeNumber.checkPrimeNumber(56);
		boolean check=primeNumber.checkPrimeNumberJ8(56);
		System.out.println("The given number is prime : "+check);
	}
}
