package com.sampleprograms.sample;

import java.io.InputStream;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PrimeNumber {
    public void checkPrimeNumber(int number){
        int flag=0;
        for(int i=2;i<number;i++){
            if(number%i==0){
                flag=+1;
            }
        }
        if(flag==0){
            System.out.println("Number is Prime");
        }
        else
            System.out.println("Number is not Prime");
    }

    public boolean checkPrimeNumberJ8(int number){
       return number>1 && IntStream.range(2,number).noneMatch(i -> number%i==0);
    }
}
